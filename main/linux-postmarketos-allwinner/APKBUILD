# Maintainer: Martijn Braam <martijn@brixit.nl>
# Co-Maintainer: Luca Weiss <luca@z3ntu.xyz>
# Co-Maintainer: Bart Ribbers <bribbers@disroot.org>
_flavor=postmarketos-allwinner
_config="config-$_flavor.$CARCH"
pkgname=linux-$_flavor
pkgver=5.8.0_git20200911
pkgrel=3
_commit="e1c26b7bd643515d3be20268cd2385df2388f8b9"
pkgdesc="Kernel fork with Pine64 patches"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/pine64-org/linux/"
license="GPL-2.0-only"
makedepends="
	bison
	devicepkg-dev
	flex
	installkernel
	openssl-dev
	perl
	rsync
	xz
	"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-anbox"
source="$pkgname-$_commit.tar.gz::https://gitlab.com/pine64-org/linux/-/archive/$_commit/linux-$_commit.tar.gz
	config-$_flavor.aarch64
	disable-power-save.patch
	touch-dts.patch
	convergence.patch
	camera-added-bggr-bayer-mode.patch
	"
subpackages="$pkgname-dev"
builddir="$srcdir/linux-$_commit"

prepare() {
	default_prepare

	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor" \
		CFLAGS_MODULE=-fno-pic
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make -j1 modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

dev() {
	provides="linux-headers"
	replaces="linux-headers"

	cd $builddir

	# https://github.com/torvalds/linux/blob/master/Documentation/kbuild/headers_install.rst
	make -j1 headers_install \
		ARCH="$_carch" \
		INSTALL_HDR_PATH="$subpkgdir"/usr
}

sha512sums="a976daf8ffeeff1d650edc48a3268e84e7ac7a1b1151311376590d2972826e24f5b4c9c2880a1f1bcb1324d5162e06be96ef74e46fd96f081193ba376957a921  linux-postmarketos-allwinner-e1c26b7bd643515d3be20268cd2385df2388f8b9.tar.gz
61e9afb2dae5edb09e4660bea66655acd0fdb283396961ef8148af8fc35e4e8e7a8fd8b8b08ec0aafc93701cf9f9b2c763551288522221d3c9af0a95c8a4c853  config-postmarketos-allwinner.aarch64
3c0d9d282a36a5f6a442b434839d77851f9b20185725cb73aee88e6e209c68fd3d71df8e2a36ffcdb605c47f86df5dbcda5d00353c75c23303861936196924e0  disable-power-save.patch
c6e1ff1c060f68a59fa57a7cfc573a500fc8d200f56193530f7c1967e4f70f17cb2c930496f6a6489a6a10de130a2e66f5cd328eb6c4ae936f4af348a7413c3b  touch-dts.patch
3e38a3d25c5981d0bb2e40cedb1ff473477be48b1ed3919e77aea23a108f12e8a3794aced53d8e96ce03f03a81dbcad59d61cff023f8714971e01e714f74b368  convergence.patch
fc708cbb12b6419858f4d4efd885bdd34eee72be78601bc9542cbd69e6ba576ac15426a75ccbc7ca57b01db735f0d097e4410afdbb9fd35a20806cdb170e4115  camera-added-bggr-bayer-mode.patch"
